package datetime

import (
	"time"
)

//http://cldr.unicode.org/translation/date-time-patterns
//https://docs.oracle.com/javase/7/docs/api/java/text/SimpleDateFormat.html
//http://www.unicode.org/reports/tr35/tr35-45/tr35-dates.html#Date_Format_Patterns

func Format(value time.Time, format string) string {
	var dst SimpleDateFormat
	dst.SetFormat(format)
	return dst.String(value)
}

type letterCount struct {
	letter byte
	count  int
}

type SimpleDateFormat struct {
	useDate bool
	useTime bool
	reserve int
	tokens  []letterCount
}

func (obj *SimpleDateFormat) SetFormat(format string) {
	// Clear internal data
	*obj = SimpleDateFormat{}

	// Setting new data
	for len(format) != 0 {
		// Get the letter and count
		letter := format[0]
		count := 1
		for i := 1; i < len(format) && format[i] == letter; i++ {
			count++
		}
		// Remove this letter(s) from format
		format = format[count:]

		// Setting need of date and time extraction to their values
		switch letter {
		case 'Y', 'M', 'd':
			obj.useDate = true
		case 'H', 'm', 's':
			obj.useTime = true
		}

		if letter == 'n' && count > 9 {
			obj.reserve += 9
		} else if letter == 'M' && count == 4 {
			obj.reserve += 10
		} else if letter == 'M' && count == 5 {
			obj.reserve++
		} else {
			obj.reserve += count
		}

		// Setting formatting tokens
		obj.tokens = append(obj.tokens, letterCount{letter: letter, count: count})
	}
}

func (obj SimpleDateFormat) Bytes(value time.Time) []byte {
	if value.IsZero() {
		return nil
	}

	return obj.Append(make([]byte, 0, obj.reserve), value)
}

func (obj SimpleDateFormat) String(value time.Time) string {
	return string(obj.Bytes(value))
}

func (obj SimpleDateFormat) Append(buffer []byte, value time.Time) []byte {
	if value.IsZero() {
		return buffer
	}

	// Date value extracted if used
	var year int
	var month time.Month
	var day int
	if obj.useDate {
		year, month, day = value.Date()
	}

	// Time value extracted if used
	var hour int
	var minute int
	var second int
	if obj.useTime {
		hour, minute, second = value.Clock()
	}

	// Formatting with tokens
	for _, v := range obj.tokens {
		switch v.letter {
		case 'Y': //Year (in "Week of Year" based calendars)
			buffer = appendYear(buffer, year, v.count)
		case 'M': //Month
			if v.count <= 2 {
				buffer = appendInt(buffer, int(month), v.count)
			} else if v.count == 3 {
				buffer = append(buffer, month.String()[:3]...)
			} else if v.count == 4 {
				buffer = append(buffer, month.String()...)
			} else if v.count == 5 {
				buffer = append(buffer, month.String()[0])
			}
		case 'd': //Day of the month
			buffer = appendInt(buffer, day, v.count)
		case 'H':
			buffer = appendInt(buffer, hour, v.count)
		case 'm':
			buffer = appendInt(buffer, minute, v.count)
		case 's':
			buffer = appendInt(buffer, second, v.count)
		case 'n':
			buffer = appendNanoSeconds(buffer, value.Nanosecond(), v.count)
		default:
			for i := 0; i < v.count; i++ {
				buffer = append(buffer, v.letter)
			}
		}
	}

	return buffer
}

func appendYear(b []byte, x int, width int) []byte {
	switch width {
	case 2:
		return appendInt(b, x%100, 2)
	case 4:
		return appendInt(b, x, 4)
	}
	for i := 0; i < width; i++ {
		b = append(b, 'Y')
	}
	return b
}

func appendNanoSeconds(b []byte, x int, width int) []byte {
	switch width {
	case 1:
		return appendInt(b, x/1e8, 1)
	case 2:
		return appendInt(b, x/1e7, 2)
	case 3:
		return appendInt(b, x/1e6, 3)
	case 4:
		return appendInt(b, x/1e5, 4)
	case 5:
		return appendInt(b, x/1e4, 5)
	case 6:
		return appendInt(b, x/1e3, 6)
	case 7:
		return appendInt(b, x/1e2, 7)
	case 8:
		return appendInt(b, x/1e1, 8)
	default:
		return appendInt(b, x, 9)
	}
}

// appendInt appends the decimal form of x to b and returns the result.
// If the decimal form (excluding sign) is shorter than width, the result is padded with leading 0's.
// Duplicates functionality in strconv, but avoids dependency.
func appendInt(b []byte, x int, width int) []byte {
	u := uint(x)
	if x < 0 {
		b = append(b, '-')
		u = uint(-x)
	}

	// Assemble decimal in reverse order.
	var buf [20]byte
	i := len(buf)
	for u >= 10 {
		i--
		q := u / 10
		buf[i] = byte('0' + u - q*10)
		u = q
	}
	i--
	buf[i] = byte('0' + u)

	// Add 0-padding.
	for w := len(buf) - i; w < width; w++ {
		b = append(b, '0')
	}

	return append(b, buf[i:]...)
}
