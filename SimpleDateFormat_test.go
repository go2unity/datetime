package datetime

import (
	"testing"
	"time"
)

func Test_NoFormat(t *testing.T) {
	time1, _ := time.Parse(time.RFC3339, "2006-01-02T15:04:05+07:00")

	if Format(time.Time{}, "") != "" {
		t.Error("No format - No value")
	}
	if Format(time1, "") != "" {
		t.Error("No format - One value")
	}
}

func Test_Year(t *testing.T) {
	time1, _ := time.Parse(time.RFC3339, "2006-01-02T15:04:05+07:00")

	var format = "Y"
	if Format(time.Time{}, format) != "" {
		t.Errorf("Format %v - No value", format)
	}
	if Format(time1, format) != "Y" {
		t.Errorf("Format %v - One value", format)
	}

	format = "YY"
	if Format(time.Time{}, format) != "" {
		t.Errorf("Format %v - No value", format)
	}
	if Format(time1, format) != "06" {
		t.Errorf("Format %v - One value", format)
	}

	format = "YYY"
	if Format(time.Time{}, format) != "" {
		t.Errorf("Format %v - No value", format)
	}
	if Format(time1, format) != "YYY" {
		t.Errorf("Format %v - One value", format)
	}

	format = "YYYY"
	if Format(time.Time{}, format) != "" {
		t.Errorf("Format %v - No value", format)
	}
	if Format(time1, format) != "2006" {
		t.Errorf("Format %v - One value", format)
	}

	format = "YYYYY"
	if Format(time.Time{}, format) != "" {
		t.Errorf("Format %v - No value", format)
	}
	if Format(time1, format) != "YYYYY" {
		t.Errorf("Format %v - One value", format)
	}
}

func Test_Month(t *testing.T) {
	time1, _ := time.Parse(time.RFC3339, "2006-01-02T15:04:05+07:00")
	time3, _ := time.Parse(time.RFC3339, "2006-10-12T15:04:05+07:00")

	format := "M"
	if Format(time.Time{}, format) != "" {
		t.Errorf("Format %v - No value", format)
	}
	if Format(time1, format) != "1" {
		t.Errorf("Format %v - One value", format)
	}
	if Format(time3, format) != "10" {
		t.Errorf("Format %v - One value", format)
	}

	format = "MM"
	if Format(time1, format) != "01" {
		t.Errorf("Format %v - One value", format)
	}

	format = "MMM"
	if Format(time1, format) != "Jan" {
		t.Errorf("Format %v - One value", format)
	}

	format = "MMMM"
	if Format(time1, format) != "January" {
		t.Errorf("Format %v - One value", format)
	}

	format = "MMMMM"
	if Format(time1, format) != "J" {
		t.Errorf("Format %v - One value", format)
	}

	format = "MMMMMM"
	if Format(time1, format) != "" {
		t.Errorf("Format %v - One value", format)
	}
}

func Test_Day(t *testing.T) {
	time1, _ := time.Parse(time.RFC3339, "2006-01-02T15:04:05+07:00")
	time3, _ := time.Parse(time.RFC3339, "2006-10-12T15:04:05+07:00")

	format := "d"
	if Format(time1, format) != "2" {
		t.Errorf("Format %v - One value", format)
	}
	if Format(time3, format) != "12" {
		t.Errorf("Format %v - One value", format)
	}

	format = "dd"
	if Format(time1, format) != "02" {
		t.Errorf("Format %v - One value", format)
	}
	if Format(time3, format) != "12" {
		t.Errorf("Format %v - One value", format)
	}

	format = "ddd"
	if Format(time1, format) != "002" {
		t.Errorf("Format %v - One value", format)
	}
	if Format(time3, format) != "012" {
		t.Errorf("Format %v - One value", format)
	}
}

func Test_SimpleDateFormat(t *testing.T) {
	time1, _ := time.Parse(time.RFC3339, "2006-01-02T15:04:05+07:00")
	time1 = time1.Add(123456789 * time.Nanosecond)

	format := "n"
	if Format(time1, format) != "1" {
		t.Errorf("Format %v - One value", format)
	}

	format = "nn"
	if Format(time1, format) != "12" {
		t.Errorf("Format %v - One value", format)
	}

	format = "nnn"
	if Format(time1, format) != "123" {
		t.Errorf("Format %v - One value", format)
	}

	format = "nnnn"
	if Format(time1, format) != "1234" {
		t.Errorf("Format %v - One value", format)
	}

	format = "nnnnn"
	if Format(time1, format) != "12345" {
		t.Errorf("Format %v - One value", format)
	}

	format = "nnnnnn"
	if Format(time1, format) != "123456" {
		t.Errorf("Format %v - One value", format)
	}

	format = "nnnnnnn"
	if Format(time1, format) != "1234567" {
		t.Errorf("Format %v - One value", format)
	}

	format = "nnnnnnnn"
	if Format(time1, format) != "12345678" {
		t.Errorf("Format %v - One value", format)
	}

	format = "nnnnnnnnn"
	if Format(time1, format) != "123456789" {
		t.Errorf("Format %v - One value", format)
	}

	format = "nnnnnnnnnn"
	if Format(time1, format) != "123456789" {
		t.Errorf("Format %v - One value", format)
	}
}

func Benchmark_SimpleDateFormat(b *testing.B) {
	timeValue := time.Now()
	var formater SimpleDateFormat
	formater.SetFormat("YYYY.MM.dd HH:mm:ss,nnn")
	for i := 0; i < b.N; i++ {
		formater.Bytes(timeValue)
	}
}

func Benchmark_GoFormat(b *testing.B) {
	timeValue := time.Now()
	for i := 0; i < b.N; i++ {
		timeValue.Format("2006.01.02 15:04:05.000")
	}
}

func Benchmark_AppendSimpleDateFormat(b *testing.B) {
	timeValue := time.Now()
	var formater SimpleDateFormat
	formater.SetFormat("YYYY.MM.dd HH:mm:ss,nnn")
	var buffer = make([]byte, 64)
	for i := 0; i < b.N; i++ {
		formater.Append(buffer, timeValue)
	}
}

func Benchmark_AppendGoFormat(b *testing.B) {
	timeValue := time.Now()
	var buffer = make([]byte, 64)
	for i := 0; i < b.N; i++ {
		timeValue.AppendFormat(buffer, "2006.01.02 15:04:05.000")
	}
}
