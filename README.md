# Installation

```sh
go get -u gitlab.com/go2unity/datetime
```

# Date and Time Patterns

| Letter | Date or Time Component | Presentation |
| :----: | :--------------------- | :----------- |
| Y      | Year                   | Year         |
| M      | Month in year          | Year         |
| d      | Day in month           | Month        |
| H      | Hour in day (0-23)     | Number       |
| m      | Minute in hour         | Number       |
| s      | Second in minute       | Number       |
| n      | Nanosecond             | Nanosecond   |

- **Year**: If the number of pattern letters is 2, the year is truncated to 2 digits; otherwise it is interpreted as a number.
- **Month**: It is interpreted following the number of pattern letters
    - 1 or 2: Interpreted as a number.
    - 3: First 3 letters of the name of the month
    - 4: Name of the month
    - 5: First letter of the name of the month
- **Number**: The number of pattern letters is the minimum number of digits, and shorter numbers are zero-padded to this amount.
- **Nanosecond**: The number of pattern letters is reduced to 9 if superior, and this defining the number of digit to be displayed
